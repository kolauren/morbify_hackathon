import pyglet

class Background():
	def __init__(self, image, batch):
		group = pyglet.graphics.Group()
		sprites = [pyglet.sprite.Sprite(image, group=group, batch=batch),
		           pyglet.sprite.Sprite(image, group=group, batch=batch, x=image.get_image_data().width)]

		self.image = image
		self.group = group
		self.sprites = sprites

	def update(self):
		pass

	def move(self, distance):
		for sprite in self.sprites:
			sprite.x -= distance

		# If the second sprite has gone past the left side, move them back 
		if self.sprites[1].x <= 0:
			self.sprites[0].x = 0
			self.sprites[1].x = self.image.get_image_data().width