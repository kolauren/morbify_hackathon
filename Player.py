import pyglet

class Player():

	ACCEL_DRIFT = -1.25
	ACCEL_LIMIT = 10
	SPEED_LIMIT = 30


	def __init__(self, images):
		self.position = 0.0
		self.speed = 1.0
		self.sprites = []
		for i in images:
			image = pyglet.image.load(i)
			self.sprites.append(pyglet.sprite.Sprite(img=image, x=300, y=50))
		self.currentIdx = 0

	def update(self):
		# Move the player's position
		self.position += self.speed
		
		# Choose frame to display based on position
		self.currentIdx = int((self.position / Player.SPEED_LIMIT) % 2)

		# Let the player drift
		self.drift()

	def accelerate(self, num):
		num = limit if num > Player.ACCEL_LIMIT else num

		# Modify speed
		num = num / 10.0

		self.speed += num
		if self.speed > Player.SPEED_LIMIT:
			self.speed = Player.SPEED_LIMIT

	def drift(self):
		# Decelerate a little bit
		self.accelerate(Player.ACCEL_DRIFT)
		if self.speed < 0:
			self.speed = 0
		pass

	def draw(self):
		self.sprites[self.currentIdx].draw()