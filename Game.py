#!/usr/bin/env python

# import ALL the things!
import pyglet
import functools

import Background
import Player

class GameState():
	def __init__(self):
		# TODO: initialize the game state?
		self.batch = pyglet.graphics.Batch()

		# Initialize background
		backgroundImg = pyglet.image.load('images/background.png')
		self.background = Background.Background(image=backgroundImg, batch=self.batch)
		self.player = Player.Player(['images/paddleguy1.png', 'images/paddleguy2.png'])

	def update(self):
		# TODO: Update all the game assets
		self.player.update()
		self.background.move(self.player.speed)

	def draw(self):
		self.batch.draw()
		self.player.draw()


# Main class: Initialize the game window, and get the game ready
class Main():
	def __init__(self, fps=60.0):
		self.window = pyglet.window.Window(width=640, height=400)
		self.window.on_draw = self.on_draw
		self.state = GameState()
		pyglet.clock.schedule_interval(self.tick, 1.0/fps)

	def tick(self, dt):
		self.state.update()

	def on_draw(self):
		self.window.clear()
		self.state.draw()
